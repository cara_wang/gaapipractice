# 參考文件

- Coding Style: http://www.php-fig.org/psr/psr-2/
- PHP framework: https://laravel.com/docs/5.1

- 開發流程:
    - https://cnyesrd.atlassian.net/wiki/pages/viewpage.action?pageId=6423698
    - https://cnyesrd.atlassian.net/wiki/pages/viewpage.action?pageId=33128474

# 關於 Google Analytics API

- Query Explorer: https://ga-dev-tools.appspot.com/query-explorer/
