<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHistoryListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historyLists', function (Blueprint $table) {
            $table->dropColumn('history');
            $table->dateTime('startDate');
            $table->dateTime('endDate');
            $table->dateTime('maxDate');
            $table->integer('maxNewVisitor');
            $table->integer('maxReturningVisitor');
            $table->dateTime('minDate');
            $table->integer('minNewVisitor');
            $table->integer('minReturningVisitor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historyLists', function (Blueprint $table) {
            $table->string('history');
            $table->dropColumn([
                'startDate',
                'endDate',
                'maxDate',
                'maxNewVisitor',
                'maxReturningVisitor',
                'minDate',
                'minNewVisitor',
                'minReturningVisitor'
            ]);
        });
    }
}
