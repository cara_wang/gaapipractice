<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>News Search for Visitors, from GA!</title>

    <meta name="description" content="News Search for Visitors, from GA!">
    <meta name="author" content="RitaLo">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            [
                '日期', 
                '新拜訪者', 
                '回訪者',
                '總拜訪者數'
            ],
            [
                "{{ Carbon\Carbon::parse(array_get($queryResult, 'maxDate'))->format('Y-m-d') }}", 
                {{ array_get($queryResult, 'maxNew') }},
                {{ array_get($queryResult, 'maxReturn') }},
                {{ array_get($queryResult, 'maxNew')+array_get($queryResult, 'maxReturn') }}
            ],
            [
                "{{ Carbon\Carbon::parse(array_get($queryResult, 'minDate'))->format('Y-m-d') }}", 
                {{ array_get($queryResult, 'minNew') }},
                {{ array_get($queryResult, 'minReturn') }},
                {{ array_get($queryResult, 'minNew')+array_get($queryResult, 'minReturn') }}
            ]
        ]);

        var options = {
            chart: {
                title: '台股拜訪者數量圖',
                subtitle: '最多拜訪者與最少拜訪者'
            }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="carousel slide" id="carousel-723978">
				<ol class="carousel-indicators">
					<li data-slide-to="0" data-target="#carousel-723978">
					</li>
					<li data-slide-to="1" data-target="#carousel-723978" class="active">
					</li>
					<li data-slide-to="2" data-target="#carousel-723978">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item">
						<img alt="Carousel Bootstrap First" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 1
							</h4>
							<p>
								成功者懂得把握最佳投資時機
							</p>
						</div>
					</div>
					<div class="item active">
						<img alt="Carousel Bootstrap Second" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 2
							</h4>
							<p>
								英鎊大跌 快湧入英國“撿便宜”
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Third" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 3
							</h4>
							<p>
								大行情將至！教您如何完美把握
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-723978" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-723978" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <a href="http://www.cnyes.com/twstock/index.htm?ga=nav"><img src="http://www.cnyes.com/images/d/twstock.gif"></a>
			<div class="page-header">
				<h1>
					台股頻道拜訪者數量查詢 <small>from Google Analytics!</small>
				</h1>
            </div>
            <button type="button" class="btn btn-default">
                <a href="/historyList">歷史查詢</a>
            </button>
            <br/><br/>
			<div class="row">
				<div class="col-md-12">
                    {!! Form::open(array('url' => 'gaSearch')) !!}
						<div class="form-group">
							 
							<label for="exampleInputEmail1">
								起始日期:
							</label>
							<input type="date" name="OrderStartDate" class="form-control" placeholder="YYYY-MM-DD">
						</div>
						<div class="form-group">
							 
							<label for="exampleInputPassword1">
								結束日期:
							</label>
							<input type="date" name="OrderEndDate" class="form-control" placeholder="YYYY-MM-DD">
						</div>
						<button type="submit" class="btn btn-default">
							Submit
						</button>
                    {!! Form::close() !!}
                    <br/><br/>

                    @if (isset($queryResult) && isset($startDate) && isset($endDate))
                        <h4>搜尋結果：</h4>
                        <table class="tg" style="margin: 0px auto;">
                            <tr>
                                <th class="tg-baqh" colspan="3">
                                    <strong>{{ $startDate }} ~ {{ $endDate }}</strong>
                                </th>
                            </tr>
                            <tr>
                                <th class="tg-s6z2"></th>
                                <th class="tg-s6z2">最多人來</th>
                                <th class="tg-s6z2">最少人來</th>
                            </tr>
                            <tr>
                                <td class="tg-s6z2">日期</td>
                                <td class="tg-s6z2">{{ Carbon\Carbon::parse(array_get($queryResult, 'maxDate'))->format('Y-m-d') }}</td>
                                <td class="tg-s6z2">{{ Carbon\Carbon::parse(array_get($queryResult, 'minDate'))->format('Y-m-d') }}</td>
                            </tr>
                            <tr>
                                <td class="tg-s6z2">新訪客數</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'maxNew') }}</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'minNew') }}</td>
                            </tr>
                            <tr>
                                <td class="tg-s6z2">回訪者數</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'maxReturn') }}</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'minReturn') }}</td>
                            </tr>
                            <tr>
                                <td class="tg-s6z2">總計</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'maxNew')+array_get($queryResult, 'maxReturn') }}</td>
                                <td class="tg-s6z2">{{ array_get($queryResult, 'minNew')+array_get($queryResult, 'minReturn') }}</td>
                            </tr>
                        </table>
                        <div id="columnchart_material" style="width: 900px; height: 500px; margin    : 0px auto;"></div>
                    @elseif ($errors->any())
                        <p class="help-block">時間格式不正確，要重新輸入唷！</p>
                    @elseif (isset($errorMessage))
                        <p class="help-block">{{ $errorMessage  }}</p>
                    @endif
                    <br/><br/>
                        @if (isset($maxNews))
                        <h4>最多人來台股頻道的當時新聞：</h4>
                            <ol>
                                @foreach ($maxNews as $key => $value)
						            <li>
							            {{ $key }} <br/> {{ $value }} 
                                    </li>
                                    <br/><br/>
                                 @endforeach
                            </ol>
                        @endif
                        
                        @if (isset($minNews))
                        <h4>最少人來台股頻道的當時新聞：</h4>
                            <ol>
                                @foreach ($minNews as $key => $value)
						            <li>
                                        {{ $key }} <br/>  {{ $value }}
                                    </li>
                                    <br/><br/>
                                @endforeach
                            </ol>
                        @endif
				</div>
			</div>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
