<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>History List</title>

    <meta name="description" content="History List">
    <meta name="author" content="Rita Lo">

    <link href="css_history/bootstrap.min.css" rel="stylesheet">
    <link href="css_history/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="carousel slide" id="carousel-707336">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-707336">
					</li>
					<li data-slide-to="1" data-target="#carousel-707336">
					</li>
					<li data-slide-to="2" data-target="#carousel-707336">
					</li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img alt="Carousel Bootstrap First" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 1
							</h4>
							<p>
								成功者懂得把握最佳投資時機
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Second" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 2
							</h4>
							<p>
								英鎊大跌 快湧入英國“撿便宜”
							</p>
						</div>
					</div>
					<div class="item">
						<img alt="Carousel Bootstrap Third" src="http://www.cnyes.com/images/cnyes_home_logo.gif?20160205">
						<div class="carousel-caption">
							<h4>
								Cnyes Logo 3
							</h4>
							<p>
								大行情將至！教您如何完美把握
							</p>
						</div>
					</div>
				</div> <a class="left carousel-control" href="#carousel-707336" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-707336" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <a href="http://www.cnyes.com/twstock/index.htm?ga=nav"><img src="http://www.cnyes.com/images/d/twstock.gif"></a>
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
			            <h1>
				            歷史查詢紀錄
			            </h1>
                    </div>
                </div>
            </div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							搜尋日期-時間
						</th>
						<th>
							Start Date
						</th>
						<th>
							End Date
                        </th>
                        <th>
                            最多人日期
                        </th>
                        <th>
                            最多人日期-新訪客數
                        </th>
                        <th>
                            最多人日期-回訪者數
                        </th>
                        <th>
                            最少人日期
                        </th>
                        <th>
                            最少人日期-新訪客數
                        </th>
                        <th>
                            最少人日期-回訪者數
                        </th>
					</tr>
				</thead>
                <tbody>
                    @foreach ($histories as $row)
                       @if ($row->id%5 == 1)
                        <tr class="warning">
                       @elseif ($row->id%5 == 2)
                        <tr class="active">
                       @elseif ($row->id%5 == 3)
                        <tr class="danger">
                       @elseif ($row->id%5 == 4)
                        <tr>
                       @elseif ($row->id%5 == 0)
                        <tr class="success">
                       @endif
                            <td>
                                {{ $row->id }}
                            </td>
                            <td>
                                {{ $row->created_at }}
                            </td>
                            <td>
                                {{ $row->startDate }}
                            </td>
                            <td>
                                {{ $row->endDate }}
                            </td>
                            <td>
                                {{ $row->maxDate }}
                            </td>
                            <td>
                                {{ $row->maxNewVisitor }}
                            </td>
                            <td>
                                {{ $row->maxReturningVisitor }}
                            </td>
                            <td>
                                {{ $row->minDate }}
                            </td>
                            <td>
                                {{ $row->minNewVisitor }}
                            </td>
                            <td>
                                {{ $row->minReturningVisitor }}
                            </td>
                        </tr>
                    @endforeach
				</tbody>
            </table>
            {!! $histories->render() !!}
            <a href="/gaSearch"><button type="button" class="btn btn-warning">HomePage</button></a>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>
