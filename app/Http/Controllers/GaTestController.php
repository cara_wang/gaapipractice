<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelAnalytics;
use App\TimeSearch;
use App\HistoryList;
use App\Http\Requests\GatestRequest;
use Carbon\Carbon;

class GaTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queryResult = [];
        return view('gaSearch', compact('queryResult'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Request Format Validation
        $this->validate($request, [
                'OrderStartDate' => 'required|date',
                'OrderEndDate'   => 'required|date'
            ]);
        
        $startDate = $request->input('OrderStartDate');
        $endDate = $request->input('OrderEndDate');
        
        // Look up whether the time period is correct
        if ( (int)$request->input('OrderStartDate') > (int)$request->input('OrderEndDate')) {
            return view('gaSearch', ['errorMessage' => '起始日期需早於結束日期唷！']);
        }

        // Query
        $query = new TimeSearch;
        $queryResult = $query->queryAllVisitors($startDate, $endDate);
        $history = new HistoryList;
        $history->recordData($startDate, $endDate, $queryResult);

        //News API
        $maxDate = Carbon::parse(array_get($queryResult, 'maxDate'))->format('Y-m-d');
        $minDate = Carbon::parse(array_get($queryResult, 'minDate'))->format('Y-m-d');
        $maxNews = $query->findDateNews($maxDate);
        $minNews = $query->findDateNews($minDate);
        
        return view('gaSearch', compact(['startDate', 'endDate', 'queryResult', 'maxNews', 'minNews']));
    }
}
