<?php

namespace App\Http\Requests;

//use App\Http\Requests\Request;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GatestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $validator = Validator::make(
            [
                'OrderStartDate' => $request->input('OrderStartDate'),
                'OrderEndDate'   => $request->input('OrderEndDate')
            ],
            $this->rules()
        );
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'OrderStartDate' => 'required|date',
            'OrderEndDate'   => 'required|date'
        ];
    }
}
