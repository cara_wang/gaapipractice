<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Log;

class HistoryList extends Model
{
    const PER_PAGE = 10;
    protected $table = 'historyLists';
    function recordData($startDate, $endDate, $queryResult)
    {
        try {
            $history = new HistoryList;
            $history->startDate = $startDate;
            $history->endDate = $endDate;
            $history->maxDate = array_get($queryResult, 'maxDate', Carbon::now()->toDateString());
            $history->maxNewVisitor = array_get($queryResult, 'maxNew', '0');
            $history->maxReturningVisitor = array_get($queryResult, 'maxReturn', '0');
            $history->minDate = array_get($queryResult, 'minDate', Carbon::now()->toDateString());
            $history->minNewVisitor = array_get($queryResult, 'minNew', '0');
            $history->minReturningVisitor = array_get($queryResult, 'minReturn', '0');
            $history->save();
        } catch(Exception $exception){
            Log::error($exception);
        }
    }

    function readData()
    {
        return HistoryList::paginate(self::PER_PAGE);
    }
}
