<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAnalytics;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Log;

class TimeSearch extends Model
{
    const METRICS = 'ga:users';
    const DIMENSIONS = 'ga:userType,ga:date';
    const SEGMENT_PREFIX = 'sessions::condition::ga:userType==';

    public function queryAllVisitors($inputStartDate, $inputEndDate)
    {
        // Query all visitors from start date to end date
        $startDate = Carbon::parse($inputStartDate);
        $endDate = Carbon::parse($inputEndDate);
        $visitorData = LaravelAnalytics::performQuery(
            $startDate,
            $endDate,
            self::METRICS,
            $others = [
                'dimensions' => 'ga:date'
            ]
        );

        // Find Maximun and Minimux Visitors Date
        foreach (array_get($visitorData, 'rows') as $visitor){
            $dateList[array_get($visitor, '0')] = array_get($visitor, '1');
        }
        $dateCollection = collect($dateList); 
        $dateSortResult = $dateCollection->sort();
        $maxDate = array_search($dateSortResult->last(), $dateList); 
        $maxDateNewVisitorNum = $this->queryNewVisitors($maxDate);
        $maxDateReturnVisitorNum = $this->queryReturningVisitors($maxDate);

        $minDate = array_search($dateSortResult->first(), $dateList);
        $minDateNewVisitorNum = $this->queryNewVisitors($minDate);
        $minDateReturnVisitorNum = $this->queryReturningVisitors($minDate);
        $array = [
            'maxDate'   => $maxDate, 
            'maxNew'    => $maxDateNewVisitorNum, 
            'maxReturn' => $maxDateReturnVisitorNum,
            'minDate'   => $minDate, 
            'minNew'    => $minDateNewVisitorNum, 
            'minReturn' => $minDateReturnVisitorNum
        ];
        return collect($array);
    }

    public function queryNewVisitors($inputDate = null)
    {
        // Query
        try {
            $startDate = Carbon::parse($inputDate);
            $endDate = Carbon::parse($inputDate);
            $newData = LaravelAnalytics::performQuery(
                $startDate,
                $endDate,
                self::METRICS,
                $others = [
                    'dimensions' => self::DIMENSIONS,
                    'segment' => self::SEGMENT_PREFIX.'New Visitor'
                ]
            );
            $result = array_get(array_get($newData, 'rows'), '0.2', '0');
        } catch (Exception $exception) {
            Log::error($exception);
        }
        return $result;
    }

    public function queryReturningVisitors($inputDate = null)    
    {
        // Query
        try {
            $startDate = Carbon::parse($inputDate);
            $endDate = Carbon::parse($inputDate);
            $returningData = LaravelAnalytics::performQuery(
                $startDate,
                $endDate,
                self::METRICS,
                $others = [
                    'dimensions' => self::DIMENSIONS,
                    'segment' => self::SEGMENT_PREFIX.'Returning Visitor'
                ]
            );
            $result = array_get(array_get($returningData, 'rows'), '0.2', '0');
        } catch (Exception $exception) {
            Log::error($exception);
        }
        return $result;
    }

    public function findDateNews($date)
    {
        try {
            $apiData = json_decode(
                file_get_contents(sprintf(config('url.newsApi'), $date, $date)),
                true
            );
            $news = [];
            $arr = array_get($apiData, 'items.data', ' ');
            foreach ($arr as $key => $value) {
                $news[array_get($arr, $key.'.title', ' ')] = array_get($arr, $key.'.summary', ' ');
            }
        } catch (Exception $exception) {
            Log::error($exception);
        }
        return $news;
    }
}

